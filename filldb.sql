-- DATAGRIP AUTO GENERATE --

create schema telegram;
create schema backend;

create extension pgcrypto;

create type telegram.component_type as enum ('image', 'document', 'text', 'link');

comment on type telegram.component_type is 'This is an interface ITabComponent<T>';

alter type telegram.component_type owner to postgres;

create table if not exists telegram.documents
(
    id         serial  not null
        constraint documents_pk
            primary key,
    name       varchar(255),
    raw        bytea   not null,
    size       integer not null,
    is_deleted timestamp,
    created    timestamp default CURRENT_TIMESTAMP
);

alter table telegram.documents
    owner to postgres;

create table if not exists telegram.texts
(
    id         serial not null
        constraint texts_pk
            primary key,
    content    text   not null,
    is_deleted timestamp,
    created    timestamp default CURRENT_TIMESTAMP
);

alter table telegram.texts
    owner to postgres;

create table if not exists telegram.images
(
    id         serial  not null
        constraint images_pk
            primary key,
    raw        bytea   not null,
    size       integer not null,
    name       text    not null,
    is_deleted timestamp,
    created    timestamp default CURRENT_TIMESTAMP
);

alter table telegram.images
    owner to postgres;

create table if not exists backend.users
(
    id         serial not null
        constraint users_pkey
            primary key,
    email      text   not null,
    first_name text   not null,
    last_name  text   not null,
    password   text   not null,
    created    timestamp default CURRENT_TIMESTAMP,
    is_deleted timestamp
);

alter table backend.users
    owner to postgres;

create table if not exists telegram.bots
(
    id          serial                  not null
        constraint bots_pk
            primary key,
    title       varchar(255)            not null,
    token       varchar(255)            not null,
    user_id     integer
        constraint bots_users_id_fk
            references backend.users,
    is_deleted  timestamp,
    created     timestamp default CURRENT_TIMESTAMP,
    is_running  boolean   default false not null,
    webhook_url text
);

alter table telegram.bots
    owner to postgres;

create unique index if not exists bots_token_uindex
    on telegram.bots (token);

create table if not exists telegram.schema
(
    id         serial  not null
        constraint schema_pk
            primary key,
    user_id    integer not null
        constraint schema_users_id_fk
            references backend.users,
    title      varchar(255),
    is_deleted timestamp,
    bot_id     integer
        constraint schema_bots_id_fk
            references telegram.bots,
    created    timestamp default CURRENT_TIMESTAMP
);

alter table telegram.schema
    owner to postgres;

create table if not exists telegram.tab
(
    id         serial not null
        constraint tab_pk
            primary key,
    title      varchar(255),
    schema_id  integer
        constraint tab_schema_id_fk
            references telegram.schema,
    is_deleted timestamp,
    created    timestamp default CURRENT_TIMESTAMP
);

alter table telegram.tab
    owner to postgres;

create table if not exists telegram.bot_users
(
    id         serial       not null
        constraint bot_users_pk
            primary key,
    name       varchar(255) not null,
    secret_id  varchar(255) not null,
    bot_id     integer
        constraint bot_users_bots_id_fk
            references telegram.bots,
    is_deleted timestamp,
    created    timestamp default CURRENT_TIMESTAMP
);

alter table telegram.bot_users
    owner to postgres;

create unique index if not exists bot_users_secret_id_uindex
    on telegram.bot_users (secret_id);

create table if not exists telegram.sessions
(
    id          serial  not null
        constraint sessions_pk
            primary key,
    current_tab integer,
    bot_user_id integer not null
        constraint sessions_bot_users_id_fk
            references telegram.bot_users,
    is_deleted  timestamp,
    created     timestamp default CURRENT_TIMESTAMP
);

alter table telegram.sessions
    owner to postgres;

create unique index if not exists sessions_bot_user_id_uindex
    on telegram.sessions (bot_user_id);

create table if not exists telegram.command
(
    id          serial       not null
        constraint command_pk
            primary key,
    name        varchar(255) not null,
    bot_id      integer      not null
        constraint command_bots_bot_id_fk
            references telegram.bots
            on delete cascade,
    link_to_tab integer      not null
        constraint command_tab_id_fk
            references telegram.tab,
    is_deleted  timestamp,
    created     timestamp default CURRENT_TIMESTAMP,
    constraint command_command_bot_id_uindex
        unique (name, bot_id)
);

alter table telegram.command
    owner to postgres;

create table if not exists telegram.links
(
    id          serial not null
        constraint links_pk
            primary key,
    next_tab_id integer
        constraint links_tab_tab_id_fk_2
            references telegram.tab,
    is_deleted  timestamp,
    created     timestamp default CURRENT_TIMESTAMP
);

alter table telegram.links
    owner to postgres;

create table if not exists telegram.tab_components
(
    tab_id         integer not null
        constraint tab_components_tab_id_fk
            references telegram.tab,
    priority       integer not null,
    component_id   integer,
    component_type telegram.component_type,
    constraint tab_id_priority_uindex
        unique (tab_id, priority)
);

alter table telegram.tab_components
    owner to postgres;


create or replace procedure telegram.execute_for_all(a_query text, a_schema text)
    language plpgsql
as
$$
declare
    t_names record;
begin
    for t_names in
        SELECT tablename
        FROM pg_catalog.pg_tables
        WHERE schemaname = a_schema
        loop
            raise notice '%', a_query;
            execute format(a_query, a_schema, t_names.tablename);
        end loop;
end;
$$;

alter procedure telegram.execute_for_all(text, text) owner to postgres;

-- Data insertion
insert into backend.users (email, first_name, last_name, password) values ('test0@gmail.com', 'Test', '0', crypt('123456', gen_salt('bf')));

insert into telegram.bots (title, token, user_id, created) values ('TestBot1', 'test::token1', (select id from backend.users where email = 'test0@gmail.com'), '2020-11-13');
insert into telegram.bots (title, token, user_id, created) values ('TestBot2', 'tokenofthetestbot::2', (select id from backend.users where email = 'test0@gmail.com'), '2020-11-13');
