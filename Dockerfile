FROM node:14

WORKDIR /node-app
ADD . /node-app

RUN npm install
CMD ["npm", "start"]
