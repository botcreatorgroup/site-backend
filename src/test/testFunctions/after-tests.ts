import { Client } from 'pg';
import * as dotenv from 'dotenv';

dotenv.config({ path: '.env.test' });

const teardown = async () => {
  const client = new Client({
    connectionString: `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_PARENT_NAME}`
  });
  await client.connect();
  await client.query(`drop database ${process.env.DB_NAME}`);
  await client.end();
};

export default teardown;
