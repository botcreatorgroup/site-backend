import { Client } from 'pg';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

dotenv.config({ path: '.env.test' });

const bootstrap = async () => {
  // Connect to parent db to drop&create test database
  const drop = async () => {
    const client = new Client({
      user: process.env.DB_USER,
      host: '91.192.105.69',
      database: process.env.DB_PARENT_NAME,
      password: process.env.DB_PASSWORD,
      port: 5432
    });

    await client.connect();
    await client.query(`drop database if exists ${process.env.DB_NAME};`);
    await client.query(`create database ${process.env.DB_NAME};`);
    await client.end();
  };

  // Connect to test db to execute sql
  const fillDb = async () => {
    const client = new Client({
      user: process.env.DB_USER,
      host: '91.192.105.69',
      database: process.env.DB_NAME,
      password: process.env.DB_PASSWORD,
      port: 5432
    });

    await client.connect();

    // Read .sql file
    const sql = fs.readFileSync('filldb.sql', 'utf-8');

    await client.query(sql);
    await client.end();
  };

  await drop();
  await fillDb();
};

export default bootstrap;
