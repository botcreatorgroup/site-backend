import App from '../../app';

describe('Bots API Tests', () => {
  process.env.NODE_ENV = 'TEST';

  test('get bots with no token in cookies', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'GET',
      url: '/api/v1/bots'
    });

    expect(response.statusCode).toBe(401);
    expect(JSON.parse(response.body)).toEqual({
      statusCode: 401,
      code: 'UNAUTHENTICATED',
      message: 'Invalid token',
      name: 'TokenError'
    });

    await fastify.app.close();
  });

  test('get bots with invalid token signature', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'GET',
      url: '/api/v1/bots',
      cookies: {
        'X-Auth-Token': 'SomeInvalidToken'
      }
    });

    expect(response.statusCode).toBe(401);
    expect(JSON.parse(response.body)).toEqual({
      statusCode: 401,
      code: 'UNAUTHENTICATED',
      message: 'Invalid token',
      name: 'TokenError'
    });

    await fastify.app.close();
  });

  // Update the response body expected object
  test('get bots with invalid payload', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const token = await fastify.app.jwt.sign({ email: 'noemail@.com' });
    const response = await fastify.app.inject({
      method: 'GET',
      url: '/api/v1/bots',
      cookies: {
        'X-Auth-Token': token
      }
    });

    expect(response.statusCode).toBe(401);
    expect(JSON.parse(response.body)).toEqual({
      statusCode: 401,
      code: 'UNAUTHENTICATED',
      message: 'Invalid token',
      name: 'TokenError'
    });

    await fastify.app.close();
  });

  test('get all user bots', async () => {
    const fastify = new App({ logger: { prettyPrint: true, level: 'debug' } });
    await fastify.buildApp();

    const payload = {
      user: {
        id: 1,
        email: 'test0@gmail.com',
        firstName: 'Test',
        lastName: '0'
      }
    };

    const token = await fastify.app.jwt.sign(payload);
    const response = await fastify.app.inject({
      method: 'GET',
      url: '/api/v1/bots',
      cookies: {
        'X-Auth-Token': token
      }
    });

    const parsedBody = JSON.parse(response.body);

    expect(parsedBody.length).toBeGreaterThanOrEqual(2);

    expect(parsedBody).toContainEqual({
      id: 1,
      title: 'TestBot1',
      token: 'test::token1',
      isRunning: false,
      created: '2020-11-12T22:00:00.000Z',
      type: 'telegram'
    });

    expect(parsedBody).toContainEqual({
      id: 2,
      title: 'TestBot2',
      token: 'tokenofthetestbot::2',
      isRunning: false,
      created: '2020-11-12T22:00:00.000Z',
      type: 'telegram'
    });

    await fastify.app.close();
  });

  // test('get user bot by token', async () => {
  //   const fastify = new App();
  //   await fastify.buildApp();
  //
  //   const token = await fastify.app.jwt.sign({ email: 'test0@gmail.com' });
  //   const response = await fastify.app.inject({
  //     method: 'GET',
  //     url: '/api/v1/bots/view/test::token1',
  //     cookies: {
  //       'X-Auth-Token': token
  //     }
  //   });
  //
  //   const parsedBody = JSON.parse(response.body);
  //
  //   expect(parsedBody.length).toBe(1);
  //   expect(parsedBody[0]).toEqual({
  //     title: 'TestBot1',
  //     token: 'test::token1',
  //     isRunning: false,
  //     activeSchema: null,
  //     created: '2020-11-12T22:00:00.000Z'
  //   });
  //
  //   await fastify.app.close();
  // });

  test('add a new bot', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const payload = {
      user: {
        id: 1,
        email: 'test0@gmail.com',
        firstName: 'Test',
        lastName: '0'
      }
    };

    const token = await fastify.app.jwt.sign(payload);
    const response = await fastify.app.inject({
      method: 'POST',
      url: '/api/v1/bots',
      cookies: {
        'X-Auth-Token': token
      },
      payload: {
        title: 'NewTestBot',
        token: 'n3wt32tb0t043n'
      }
    });

    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.body)).toEqual({
      msg: 'Bot was successfully created'
    });

    await fastify.app.close();
  });
});
