import App from '../../app';

// Route: /register
describe('API Authentication Tests', () => {
  process.env.NODE_ENV = 'TEST';

  test('create a new user', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: `/api/v1/auth/register`,
      payload: {
        email: 'test@test.com',
        password: 'testtest',
        firstName: 'Test',
        lastName: 'Test'
      }
    });

    expect(JSON.parse(response.body)).toEqual({
      msg: 'User successfully created'
    });
    expect(response.cookies.length).toBe(1);

    const [cookie] = response.cookies;

    expect(cookie).toEqual(
      expect.objectContaining({
        name: 'X-Auth-Token',
        value: expect.any(String),
        domain: process.env.HOST,
        path: '/',
        sameSite: 'Strict',
        expires: expect.any(Date)
      })
    );

    const token = (cookie as { value: string }).value;
    const payload = await fastify.app.jwt.decode(token);

    expect(payload).toEqual({
      user: {
        id: 2,
        email: 'test@test.com',
        firstName: 'Test',
        lastName: 'Test'
      }
    });

    await fastify.app.close();
  });

  test('registering user already exists', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: '/api/v1/auth/register',
      payload: {
        email: 'test0@gmail.com',
        password: 'testtest',
        firstName: 'Test',
        lastName: '0'
      }
    });

    expect(response.statusCode).toBe(409);
    expect(JSON.parse(response.body)).toEqual({
      code: 409,
      status: 'ALREADY_EXISTS',
      message: 'User with the same email already exists'
    });

    await fastify.app.close();
  });

  test('registering user with invalid email', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: '/api/v1/auth/register',
      payload: {
        email: 'test@.com',
        password: 'test123',
        firstName: 'Test123',
        lastName: '123Test'
      }
    });

    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.body)).toEqual({
      code: 400,
      status: 'INVALID_ARGUMENT',
      message: 'Invalid email'
    });

    await fastify.app.close();
  });

  test('registering user with too short password', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: '/api/v1/auth/register',
      payload: {
        email: 'testp@test.com',
        password: 'noten',
        firstName: 'Test',
        lastName: 'Test'
      }
    });

    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.body)).toEqual({
      code: 400,
      status: 'INVALID_ARGUMENT',
      message: 'Password must contain at least 6 symbols'
    });

    await fastify.app.close();
  });

  // Route: /login
  test('successful login', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: `/api/v1/auth/login`,
      payload: {
        email: 'test0@gmail.com',
        password: '123456'
      }
    });

    expect(JSON.parse(response.body)).toEqual({
      msg: 'User successfully authenticated'
    });
    expect(response.cookies.length).toBe(1);

    const [cookie] = response.cookies;

    expect(cookie).toEqual(
      expect.objectContaining({
        name: 'X-Auth-Token',
        value: expect.any(String),
        domain: process.env.HOST,
        path: '/',
        sameSite: 'Strict',
        expires: expect.any(Date)
      })
    );

    const token: string = (cookie as { value: string }).value;
    const payload = await fastify.app.jwt.decode(token);

    expect(payload).toEqual({
      user: {
        id: 1,
        email: 'test0@gmail.com',
        firstName: 'Test',
        lastName: '0'
      }
    });

    await fastify.app.close();
  });

  test('user not exists', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: `/api/v1/auth/login`,
      payload: {
        email: 'notest@gmail.com',
        password: 'somepassword'
      }
    });

    expect(response.statusCode).toBe(404);
    expect(JSON.parse(response.body)).toEqual({
      code: 404,
      status: 'NOT_FOUND',
      message: 'User does not exist'
    });

    await fastify.app.close();
  });

  test('login with wrong password', async () => {
    const fastify = new App();
    await fastify.buildApp();

    const response = await fastify.app.inject({
      method: 'POST',
      url: `/api/v1/auth/login`,
      payload: {
        email: 'test0@gmail.com',
        password: 'idontknowthepassword'
      }
    });

    expect(response.statusCode).toBe(401);
    expect(JSON.parse(response.body)).toEqual({
      code: 401,
      status: 'UNATHENTICATED',
      message: 'Invalid email or password'
    });

    await fastify.app.close();
  });
});
