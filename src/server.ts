import App from './app';

const start = async () => {
  const app = new App({ logger: { prettyPrint: true, level: 'debug' } });
  const server = await app.buildApp();
  await server.listen(
    process.env.PORT as string,
    process.env.HOST as string,
    (err: Error, address: string) => {
      if (err) process.exit(1);
    }
  );
};

start();

