import PostgresDatabase from './global/database/postgresDatabase';

class Api {
  database: PostgresDatabase;

  constructor(database: PostgresDatabase) {
    this.database = database;
  }
}

export default Api;
