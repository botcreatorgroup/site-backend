import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import validator from 'validator';
import { User } from '../../../global/typings/entities';
import Api from '../../../api';

export const uniqueUserValidation = (api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { email } = request.body as Pick<User, 'email'>;

  // Check if the user already exists
  const model = await api.database.selectUserByEmail(email);

  if (model)
    return reply.status(409).send({
      code: 409,
      status: 'ALREADY_EXISTS',
      message: 'User with the same email already exists'
    });
};

export const userExistsValidation = (api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { email } = request.body as Pick<User, 'email'>;

  // Check if the user with email exists
  const model = await api.database.selectUserByEmail(email);

  if (!model)
    return reply.status(404).send({
      code: 404,
      status: 'NOT_FOUND',
      message: 'User does not exist'
    });
};

export const emailValidation = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { email } = request.body as User;
  if (!validator.isEmail(email))
    reply.code(400).send({
      code: 400,
      status: 'INVALID_ARGUMENT',
      message: 'Invalid email'
    });
};

export const passwordLengthValidation = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { password } = request.body as User;
  if (password && !validator.isLength(password as string, { min: 6, max: 72 }))
    reply.code(400).send({
      code: 400,
      status: 'INVALID_ARGUMENT',
      message: 'Password must contain at least 6 symbols'
    });
};
