import { FastifyInstance } from 'fastify';
import * as createError from 'fastify-error';
import { User, JWT } from '../../../global/typings/entities';

export async function userDecorator(fastify: FastifyInstance): Promise<User> {
  const token = this.cookies['X-Auth-Token'];

  const TokenError = createError('UNAUTHENTICATED', 'Invalid token', 401);

  if (!token) {
    throw new TokenError();
  }

  const payload = fastify.jwt.decode(token);

  if (payload) {
    const { user } = payload as JWT;

    if (!user) {
      throw new TokenError();
    }
    return user;
  }

  throw new TokenError();
}
