import { FastifyInstance } from 'fastify';

import Api from '../../../api';
import {
  selectSchemas,
  createSchema,
  updateSchema,
  deleteSchema
} from '../controllers/schemas';

const defineSchemaRoutes = async (fastify: FastifyInstance, api: Api) => {
  // Select schemas route
  fastify.get('/api/v1/schemas', {
    handler: selectSchemas(fastify, api)
  });

  // Create schema route
  fastify.post('/api/v1/schemas', {
    schema: {
      body: {
        type: 'object',
        required: ['title'],
        properties: {
          title: { type: 'string' }
        }
      }
    },
    handler: createSchema(fastify, api)
  });

  // Update schema route
  fastify.patch('/api/v1/schemas', {
    handler: updateSchema(fastify, api)
  });

  // Delete schema route
  fastify.delete('/api/v1/schemas', {
    handler: deleteSchema(fastify, api)
  });
};

export default defineSchemaRoutes;
