import { FastifyInstance } from 'fastify';

import Api from '../../../api';
import { registerUser, loginUser } from '../controllers/auth';
import {
  passwordLengthValidation,
  emailValidation,
  uniqueUserValidation,
  userExistsValidation
} from '../hooks/preValidationAuth';

const defineAuthRoutes = async (fastify: FastifyInstance, api: Api) => {
  // Login route
  fastify.post('/api/v1/auth/login', {
    schema: {
      body: {
        type: 'object',
        properties: {
          email: { type: 'string' },
          password: { type: 'string' }
        }
      }
    },
    preValidation: [emailValidation, passwordLengthValidation, userExistsValidation(api)],
    handler: loginUser(fastify, api)
  });

  // Register route
  fastify.post('/api/v1/auth/register', {
    schema: {
      body: {
        type: 'object',
        properties: {
          email: { type: 'string' },
          firstName: { type: 'string' },
          lastName: { type: 'string' },
          password: { type: 'string' }
        }
      }
    },
    preValidation: [emailValidation, passwordLengthValidation, uniqueUserValidation(api)],
    handler: registerUser(fastify, api)
  });
};

export default defineAuthRoutes;
