import { FastifyInstance } from 'fastify';

import Api from '../../../api';
import {
  createBot,
  listBots,
  updateBot,
  deleteBot
} from '../controllers/bots';

const defineBotRoutes = async (fastify: FastifyInstance, api: Api) => {
  // Select Bots route
  fastify.get('/api/v1/bots', {
    handler: listBots(fastify, api)
  });

  // Create bot route
  fastify.post('/api/v1/bots', {
    schema: {
      body: {
        type: 'object',
        required: ['title', 'token'],
        properties: {
          title: { type: 'string' },
          token: { type: 'string' }
        }
      }
    },
    handler: createBot(fastify, api)
  });

  // Update bot route
  fastify.patch('/api/v1/bots', {
    handler: updateBot(fastify, api)
  });

  // Delete bot route
  fastify.delete('/api/v1/bots', {
    handler: deleteBot(fastify, api)
  });
};

export default defineBotRoutes;
