import { FastifyInstance } from 'fastify';

import Api from '../../../api';
import {
  selectTabs,
  createTab,
  deleteTab,
  updateTab
} from '../controllers/tabs';


const defineTabRoutes = async (fastify: FastifyInstance, api: Api) => {
  // Select tabs
  fastify.get('/api/v1/schemas/:schemaId/tabs', {
    handler: selectTabs(fastify, api)
  });

  // Create tab
  fastify.post('/api/v1/schemas/:schemaId/tabs', {
    schema: {
      body: {
        type: 'object',
        required: ['title'],
        properties: {
          title: { type: 'string' }
        }
      }
    },
    handler: createTab(fastify, api)
  });

  // Update tab
  fastify.patch('/api/v1/tabs/:tabId', {
    handler: updateTab(fastify, api)
  });

  // Delete tab
  fastify.delete('/api/v1/tabs/:tabId', {
    handler: deleteTab(fastify, api)
  });
};

export default defineTabRoutes;
