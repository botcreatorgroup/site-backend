import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { User } from '../../../global/typings/entities';

import Api from '../../../api';

export const registerUser = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { email, firstName, lastName, password } = request.body as User;

  // Save user to the DB
  await api.database.createUser({ email, firstName, lastName, password });

  // Generate a user token
  const user: User = await api.database.selectUserByEmail(email);
  const token = fastify.jwt.sign({ user }, { noTimestamp: true });

  return reply
    .setCookie('X-Auth-Token', token, {
      httpOnly: false,
      domain: process.env.HOST,
      path: '/',
      sameSite: true,
      expires: new Date(Date.now() + 60 * 60 * 1000)
    })
    .send({ msg: 'User successfully created' });
};

export const loginUser = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { email, password } = request.body as Pick<User, 'email' | 'password'>;

  fastify.log.debug(`Login Route: ${api.database}`);

  // Check password
  if ((await api.database.validatePassword({ email, password })) === false) {
    return reply.status(401).send({
      code: 401,
      status: 'UNATHENTICATED',
      message: 'Invalid email or password'
    });
  }

  // Generate a user token
  const user: User = await api.database.selectUserByEmail(email);
  const token = fastify.jwt.sign({ user }, { noTimestamp: true });

  return reply
    .setCookie('X-Auth-Token', token, {
      httpOnly: false,
      domain: process.env.HOST,
      path: '/',
      sameSite: true,
      expires: new Date(Date.now() + 60 * 60 * 1000)
    })
    .send({ msg: 'User successfully authenticated' });
};
