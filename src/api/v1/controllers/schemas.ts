import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { Schema, User } from '../../../global/typings/entities';

import Api from '../../../api';

// TODO Create selectSchemaById controller
export const selectSchemas = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { id } = (await request.user) as User;

  const schema = await api.database.selectSchemaByUserId(id as number);

  reply.send(schema);
};

export const createSchema = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { id } = (await request.user) as User;
  const { title } = request.body as Pick<Schema, 'title'>;

  await api.database.createSchema({ userId: id as number, title });

  reply.send({ message: 'Schema successfully created' });
};

export const updateSchema = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const schema = request.body as Pick<Schema, 'id' | 'title'>;

  await api.database.updateSchema(schema);

  reply.send({ message: 'Schema was successfully updated' });
};

export const deleteSchema = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { id } = request.body as Pick<Schema, 'id'>;

  const date = new Date();
  const month =
    date.getMonth() < 9 ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;
  const isDeleted = `${date.getFullYear()}-${month}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;

  await api.database.updateSchema({ id, isDeleted });
  reply.send({ message: 'Schema was successfully deleted' });
};
