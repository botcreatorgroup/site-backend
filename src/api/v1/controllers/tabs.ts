import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { Tab } from '../../../global/typings/entities';

import Api from '../../../api';

// TODO Create selectTabById controller
export const selectTabs = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { schemaId } = request.params as { schemaId: number };

  const tabs = await api.database.selectTabsBySchemaId(schemaId);

  reply.send(tabs);
};

export const createTab = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { schemaId } = request.params as { schemaId: number };
  const { title } = request.body as Pick<Tab, 'title'>;

  await api.database.createTab({ schemaId, title });
  reply.send({ message: 'Tab was successfully created' });
};

export const updateTab = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { tabId } = request.params as { tabId: number };
  const { schemaId, title } = request.body as Pick<Tab, 'title' | 'schemaId'>;

  await api.database.updateTab({ id: tabId, schemaId, title });
  reply.send({ message: 'Tab was successfully updated' });
};

export const deleteTab = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { tabId } = request.params as { tabId: number };

  const date = new Date();
  const month =
    date.getMonth() < 9 ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;
  const isDeleted = `${date.getFullYear()}-${month}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;

  await api.database.updateTab({ id: tabId, isDeleted });
  reply.send({ message: 'Tab was successfully deleted' });
};
