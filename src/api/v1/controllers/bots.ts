import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { User, Bot } from '../../../global/typings/entities';

import Api from '../../../api';

export const listBots = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const user = (await request.user) as User;

  // TODO Set the bot type in the model
  const bots = (await api.database.selectBotsByUserId(user.id as number)).map(
    (bot: Bot) => ({
      ...bot,
      type: 'telegram'
    })
  );

  return reply.send(bots);
};

export const createBot = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const user = (await request.user) as User;
  const bot = request.body as Pick<Bot, 'title' | 'token'>;

  // TODO Set a webhook on the bot

  // await axios.post(`${process.env.HANDLER_PROXY}/handler/webhook/create`, {
  //   token: bot.token
  // });

  await api.database.createBot(bot, user.id as number);

  return reply.send({ msg: 'Bot was successfully created' });
};

export const updateBot = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  // TODO Create a type for bot updating
  const bot: Partial<Bot> & Pick<Bot, 'id'> = request.body as Partial<Bot> &
    Pick<Bot, 'id'>;

  await api.database.updateBot(bot);

  reply.send({ msg: 'Bot was successfully updated' });
};

export const deleteBot = (fastify: FastifyInstance, api: Api) => async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const { id } = request.body as Pick<Bot, 'id'>;

  const date = new Date();
  const month =
    date.getMonth() < 9 ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;
  const isDeleted = `${date.getFullYear()}-${month}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;

  // TODO Check whether a bot is already deleted
  await api.database.updateBot({ id, isDeleted });

  reply.send({ msg: 'Bot was successfully deleted' });
};
