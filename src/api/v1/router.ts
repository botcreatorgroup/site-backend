import { FastifyInstance } from 'fastify';
import Api from '../../api';

import defineAuthRoutes from './routes/auth';
import defineBotsRoutes from './routes/bots';
import defineSchemaRoutes from './routes/schemas';
import defineTabsRoutes from './routes/tabs';
import { User } from '../../global/typings/entities';
import { userDecorator } from './decorators/user';

const defineAllRoutes = async (fastify: FastifyInstance, api: Api) => {
  fastify.decorateRequest('user', {
    getter(): Promise<User> {
      return userDecorator.call(this, fastify);
    }
  });

  await defineAuthRoutes(fastify, api);
  await defineBotsRoutes(fastify, api);
  await defineSchemaRoutes(fastify, api);
  await defineTabsRoutes(fastify, api);
};

export default defineAllRoutes;
