import fastify from 'fastify';
import * as dotenv from 'dotenv';

import Api from './api';
import globalPlugin from './global/plugins';
import defineAllRoutes from './api/v1/router';

import { FastifyInstance, FastifyServerOptions } from 'fastify';
import PostgresDatabase from './global/database/postgresDatabase';
import { DotenvConfigOptions } from 'dotenv';

class App {
  readonly app: FastifyInstance;

  constructor(options: FastifyServerOptions = {}) {
    this.app = fastify(options);
  }

  private async loader() {
    await this.app.register(globalPlugin);

    const postgresDatabase = new PostgresDatabase(this.app.pg);
    const api = new Api(postgresDatabase);

    this.app.log.debug(typeof api.database);

    await defineAllRoutes(this.app, api);
  }

  public async buildApp() {
    let options: DotenvConfigOptions;

    if (process.env.NODE_ENV === 'DEV') options = { path: '.env' };
    else if (process.env.NODE_ENV === 'TEST') options = { path: '.env.test' };
    else {
      this.app.log.error('Unknown configuration');
      process.exit(1);
    }

    dotenv.config(options);

    await this.loader();
    return this.app;
  }
}

export default App;
