import fastifyPlugin from 'fastify-plugin';

import cookiePlugin from './plugins/cookie';
import jwtPlugin from './plugins/jwt';
import postgresPlugin from './plugins/postgres';
import corsPlugin from './plugins/cors';

import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const globalPlugin = async (
  fastify: FastifyInstance,
  options: FastifyRegisterOptions<any>
) => {
  fastify.register(postgresPlugin);
  fastify.register(corsPlugin);
  fastify.register(cookiePlugin);
  fastify.register(jwtPlugin);
};

export default fastifyPlugin(globalPlugin);

