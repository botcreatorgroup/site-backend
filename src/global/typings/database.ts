import { Bot, User, Schema, Tab } from './entities';

interface Database {
  // User queries
  selectUserByEmail(email: string): Promise<User>;
  createUser(user: User): Promise<void>;
  updateUser(user: Partial<User>): Promise<void>;
  validatePassword(
    credentials: Pick<User, 'email' | 'password'>
  ): Promise<boolean>;

  //Bots queries
  selectBotsByUserId(userId: number): Promise<Array<Bot>>;
  selectBotByToken(token: string): Promise<Bot>;
  createBot(bot: Pick<Bot, 'title' | 'token'>, userId: number): Promise<void>;
  updateBot(bot: Partial<Bot>): Promise<void>;

  // Schemas queries
  selectSchemaByUserId(userId: number): Promise<Array<Schema>>;
  createSchema(schema: Pick<Schema, 'userId' | 'title'>): Promise<void>;
  updateSchema(schema: Partial<Schema>): Promise<void>;

  // Tabs queries
  selectTabsBySchemaId(schemaId: number): Promise<Array<Tab>>;
  createTab(tab: Pick<Tab, 'schemaId' |'title'>): Promise<void>;
  updateTab(tab: Pick<Tab, 'id'> & Partial<Tab>): Promise<void>;
}

export default Database;
