export interface User {
  id?: number;
  email: string;
  firstName: string;
  lastName: string;
  password?: string;
}

export interface Bot {
  id?: number;
  // userId: number;
  title: string;
  token: string;
  isRunning: boolean;
  isDeleted: Date | string;
  created: Date;
  activeSchema: number;
}

export interface Schema {
  id?: number;
  userId: number;
  title: string;
  isDeleted: Date | string;
  created: Date;
}

export interface Tab {
  id?: number;
  title: string;
  schemaId: number;
  isDeleted?: Date | string;
  created?: Date;
}

export interface JWT {
  user: User;
}
