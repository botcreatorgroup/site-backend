import { PostgresDb } from 'fastify-postgres';
import { Bot, Schema, Tab, User } from '../typings/entities';
import Database from '../typings/database';

class PostgresDatabase implements Database {
  private pg: PostgresDb;

  constructor(pg: PostgresDb) {
    this.pg = pg;
  }

  async selectUserByEmail(email: string): Promise<User> {
    const { rows } = await this.pg.query(
      `select id, email, first_name as "firstName", last_name as "lastName"
       from backend.users
       where email=$1`,
      [email]
    );

    return rows[0];
  }

  async createUser(user: User): Promise<void> {
    const { email, firstName, lastName, password } = user;

    const sql = `insert into backend.users (email, first_name, last_name, password) 
      values ($1, $2, $3, crypt($4, gen_salt('bf')))`;
    await this.pg.query(sql, [email, firstName, lastName, password]);
  }

  async updateUser(user: Partial<User>): Promise<void> {
    // TODO Parameterize the query to avoid SQL Injection
    let sql = `update backend.user set `;

    const { id, email, firstName, lastName, password } = user;
    if (email) sql += `email = '${email}' `;
    if (firstName) sql += `first_name = '${firstName}' `;
    if (lastName) sql += `last_name = '${lastName}' `;
    if (password) sql += `password = crypt('${password}', gen_salt('bf'))`;
    sql += `where id = $1`;

    await this.pg.query(sql, [id]);
  }

  async validatePassword(
    credentials: Pick<User, 'email' | 'password'>
  ): Promise<boolean> {
    const { email, password } = credentials;

    const sql = `select id from backend.users where email=$1 and password=crypt($2, password)`;
    const { rows } = await this.pg.query(sql, [email, password]);

    return !!rows.length;
  }

  async selectBotsByUserId(userId: number): Promise<Array<Bot>> {
    const { rows } = await this.pg.query(
      `select id, title, token, is_running as "isRunning", created,
       is_deleted as "isDeleted", active_schema as "activeSchema" from telegram.bots 
      where user_id = $1`,
      [userId]
    );

    return rows;
  }

  async selectBotByToken(token: string): Promise<Bot> {
    const { rows } = await this.pg.query(
      `select id, title, token, is_running as 'isRunning', created,
       is_deleted as "isDeleted", active_schema as "activeSchema" from telegram.bots
      where token=$1`,
      [token]
    );

    return rows[0];
  }

  async createBot(
    bot: Pick<Bot, 'title' | 'token'>,
    userId: number
  ): Promise<void> {
    const { title, token } = bot;

    await this.pg.query(
      `insert into telegram.bots (title, token, user_id) 
      values ($1, $2, $3)`,
      [title, token, userId]
    );
  }

  async updateBot(bot: Partial<Bot>): Promise<void> {
    const { id, title, token, isRunning, isDeleted, activeSchema } = bot;

    // TODO Parameterize the query to avoid SQL Injection
    let sql = `update telegram.bots set `;
    if (title) sql += `title = '${title}' `;
    if (token) sql += `token = '${token}' `;
    if (isRunning) sql += `is_running = ${isRunning} `;
    if (isDeleted) sql += `is_deleted = '${isDeleted}' `;
    if (activeSchema) sql += `active_schema = ${activeSchema} `;
    sql += `where id = $1`;

    await this.pg.query(sql, [id]);
  }

  async selectSchemaByUserId(userId: number): Promise<Array<Schema>> {
    const sql = `select id, user_id as "userId", title, is_deleted as "isDeleted", created 
      from telegram.schema where user_id = $1`;
    const { rows } = await this.pg.query(sql, [userId]);
    return rows;
  }

  async createSchema(schema: Pick<Schema, 'userId' | 'title'>): Promise<void> {
    const { userId, title } = schema;

    const sql = `insert into telegram.schema (user_id, title) values ($1, $2)`;
    await this.pg.query(sql, [userId, title]);
  }

  async updateSchema(schema: Partial<Schema>): Promise<void> {
    const { id, title, isDeleted } = schema;

    // TODO Parameterize the query to avoid SQL Injection
    let sql = `update telegram.schema set `;
    if (title) sql += `title = '${title}' `;
    if (isDeleted) sql += `is_deleted = '${isDeleted}' `;
    sql += `where id = $1`;

    await this.pg.query(sql, [id]);
  }

  async selectTabsBySchemaId(schemaId: number): Promise<Array<Tab>> {
    const sql = `select id, schema_id as "schemaId", title, is_deleted as "isDeleted", created
      from telegram.tab where schema_id = $1`;

    const { rows } = await this.pg.query(sql, [schemaId]);
    return rows;
  }

  async createTab(tab: Pick<Tab, 'schemaId' | 'title'>): Promise<void> {
    const { schemaId, title } = tab;

    const sql = `insert into telegram.tab (schema_id, title) values ($1, $2)`;
    await this.pg.query(sql, [schemaId, title]);
  }

  async updateTab(tab: Pick<Tab, 'id'> & Partial<Tab>): Promise<void> {
    const { id, schemaId, title, isDeleted } = tab;

    // TODO Parameterize the query to avoid SQL Injection
    let sql = `update telegram.tab set `;
    if (title) sql += `title = '${title}' `;
    if (schemaId) sql += `schema_id = ${schemaId} `;
    if (isDeleted) sql += `is_deleted = '${isDeleted}' `;
    sql += `where id = $1`;

    await this.pg.query(sql, [id]);
  }
}

export default PostgresDatabase;
