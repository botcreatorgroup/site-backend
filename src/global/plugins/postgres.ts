import fastifyPlugin from 'fastify-plugin';
import fastifyPostgres from 'fastify-postgres';
import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const dbConnector = async (
  fastify: FastifyInstance,
  options: FastifyRegisterOptions<any>
) => {
  fastify.register(fastifyPostgres, {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: Number.parseInt(process.env.DB_PORT as string)
  });
};

export default fastifyPlugin(dbConnector);
