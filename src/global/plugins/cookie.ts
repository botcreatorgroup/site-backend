import fastifyPlugin from 'fastify-plugin';
import fastifyCookie from 'fastify-cookie';
import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const cookiePlugin = async (
  fastify: FastifyInstance,
  options: FastifyRegisterOptions<any>
) => {
  fastify.register(fastifyCookie, { });
};

export default fastifyPlugin(cookiePlugin);
