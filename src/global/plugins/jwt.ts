import fastifyPlugin from 'fastify-plugin';
import fastifyJWT from 'fastify-jwt';
import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const jwt = async (
  fastify: FastifyInstance,
  options: FastifyRegisterOptions<any>
) => {
  fastify.register(fastifyJWT, {
    secret: process.env.JWT_SECRET as string,
    cookie: {
      cookieName: 'X-Auth-Token'
    }
  });
};

export default fastifyPlugin(jwt);
