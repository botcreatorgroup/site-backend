import fastifyCors from 'fastify-cors';
import fastifyPlugin from 'fastify-plugin';
import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const cors = async (
  fastify: FastifyInstance,
  opts: FastifyRegisterOptions<any>
) => {
  fastify.register(fastifyCors, {
    origin: (origin, cb) => {
      cb(null, true);
    },
    credentials: true
  });
};

export default fastifyPlugin(cors);
